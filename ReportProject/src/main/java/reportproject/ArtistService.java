/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package reportproject;

import java.util.List;

/**
 *
 * @author Nobpharat
 */
public class ArtistService {
	public List<ArtistReport> getToptenArtistByToTalPrice(){
		ArtistDao artistDao =  new ArtistDao();
		return artistDao.getArtistByToTalPrice(10);
	}
	
	public List<ArtistReport> getToptenArtistByToTalPrice(String begin ,String end){
		ArtistDao artistDao =  new ArtistDao();
		return artistDao.getArtistByToTalPrice(begin,end,10);
	}
}
